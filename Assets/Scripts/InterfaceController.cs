﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class InterfaceController : MonoBehaviour {

	[SerializeField]
	private List<GameObject> _animatedObjects;
	[SerializeField]
	private RectTransform _scrollViewContent;
	[SerializeField]
	private GameObject _scrollViewCellPrefab;

	//
	private int _currentModelID;
	private bool _isAnimating = true;

	void Start()
	{
		InitScrollViewCells();
	}

	private void InitScrollViewCells()
	{
		for(int i = 0; i < _animatedObjects.Count; i++)	{
			
			GameObject objCell = MonoBehaviour.Instantiate(_scrollViewCellPrefab);
			objCell.transform.SetParent(_scrollViewContent);
			objCell.transform.localScale = Vector3.one;
			objCell.transform.localPosition = Vector3.zero;
			objCell.GetComponent<CellView>().BuildCell(_animatedObjects[i].name, i, new UnityAction<int>(ShowModel));
		}

	}

	private void ShowModel(int idModel)
	{
		StopAnimationForCurrentObject();
		_animatedObjects[_currentModelID].SetActive(false);

		_currentModelID = idModel;
		_animatedObjects[idModel].SetActive(true);

	}

	public void AnimateObject()
	{
		if(!_isAnimating)	{
			StartAnimationForCurrentObject();
		}
		else {
			StopAnimationForCurrentObject();
		}
	}

	private void StartAnimationForCurrentObject()
	{
		_animatedObjects[_currentModelID].GetComponent<Animator>().SetTrigger("StartWalk");
		_isAnimating = true;
	}

	private void StopAnimationForCurrentObject()
	{
		_animatedObjects[_currentModelID].GetComponent<Animator>().SetTrigger("StopWalk");
		_isAnimating = false;
	}
}
