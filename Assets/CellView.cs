﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;

public class CellView : MonoBehaviour {

	[SerializeField]
	private Image _previewImage;
	[SerializeField]
	private Text _objectNameText;
	[SerializeField]
	private Button _cellButton;

	private int _modelId;

	public void BuildCell(string objName, int objId, UnityAction<int> method)	{

		_modelId = objId;

		SetObjectName(objName);
		SetPreviewImage(objName);
		SetListener(method);
	}

	private void SetObjectName(string objName)	{
		_objectNameText.text = objName;
	}

	private void SetPreviewImage(string objName)	{
		_previewImage.sprite = Resources.Load<Sprite>("PreviewImages/" + objName);
	}

	private void SetListener(UnityAction<int> method)	{

		_cellButton.onClick.AddListener( () => method(_modelId));
		//_cellButton.onClick.AddListener(method);
	}
}
